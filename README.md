# README #

* Quick summary
SF movie service for Uber's coding challenge.

* Version
1.0

### How do I get set up? ###

This is a server side project. To deploy, you need to have maven and jdk installed on your environment.

1. Go to YOUR/CHECK/OUT/REPO

2. run this command:	mvn clean compile exec:java	

3. You will see logs popping up. Especially, there will be some logging like "loading movie's data : XXX". Wait for some time (around 1 min) till you see this line "Hit enter to stop it..."
If you don't make it, try control + 'c' and do that again.

4. Do not close or stop last step. Open your browser, try to access to following endpoints:
   a.	http://localhost:8080/sfMovie/autoComplete?prefix=gu        			(change 'gu' to any value)
   b.	http://localhost:8080/sfMovie/getLocation?title=180						(change '180' to any value)

5. Not really enjoyed with http calls?
   Front end is available here: (just a simple IOS implementation)
   https://roycao2011@bitbucket.org/roycao2011/sfmovie-ios.git

### Who do I talk to? ###

If you have any question, feel free to contact me: Roy Cao <silencetsao@gmail.com>

* Repo owner or admin
* Other community or team contact