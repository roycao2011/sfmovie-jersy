/**
 * Copyright (C) 2014 Roy Cao.  All Rights Reserved.
 * Proprietary and confidential.
 * @author  Roy Cao <silencetsao@gmail.com>
 */

package com.roy.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.lang.String;

import com.roy.workflowservice.*;
import com.roy.core.*;
import com.roy.service.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *  This is endpoint controller for sfMovie service. All Http calls looks for this file, and find solutions
 *  based on URL path.
 *  
 *  URL path begins with 'sfMovie' will direct to this file
 */
@Path("sfMovie/")
public class MovieEndpointController {

    /**
     *  This is endpoint controller for URL:    http://localhost:8080/sfMovie/autoComplete?prefix=a
     *  Returns all possible titles that contains given prefix.
     *  e.g.
     *  {
     *       "result": [
     *          "Birdman of Alcatraz",
     *          "Confessions of a Burning Man",
     *          "The Last of the Gladiators",
     *          "Shadow of the Thin Man",
     *          "Memoirs of an Invisible Man"
     *       ]
     *  }    
     *  
     *  
     *  @Param  prefix   incomplete string that could be prefix of a word in title
     *   
     *  @return          String response with a json format, list of all movie titles contains prefix
     */   
    @GET
    @Path("autoComplete")
    @Produces("application/json")
    public String getByPrefix(@QueryParam("prefix") String prefix) throws Exception {

        List<String> resultList = MovieServiceController.searchMovieByPrefix(prefix);

        JSONObject json = new JSONObject();
        json.put("result", resultList);

        return json.toString();
    }


    /**
     *  This is endpoint controller for URL:    http://localhost:8080/sfMovie/getLocation?title=180
     *  Return all possible locations given movie title.
     *  e.g.
     *  {
     *     "result": [
     *          "Polk+Larkin+Streets",
     *          "City+Hall",
     *          "Justin+Herman+Plaza",
     *          "Mason+California+Streets",
     *          "Epic+Roasthouse",
     *          "200+block+Market+Street",
     *          "555+Market+St.",
     *          "Randall+Musuem"
     *      ]
     *   } 
     *
     *  Some processing on address: (adjust to Google map api call format)
     *  1. replace space ' ' with '+'
     *  2. cut off all strings after '('
     *  3. cut off last '+', if possible
     *  
     *  @Param  title   complete movie title name
     *   
     *  @return         String response with a json format, list of all locations for this movie
     */  
    @GET
    @Path("getLocation")
    @Produces("application/json")
    public String getLocationByTitle(@QueryParam("title") String title) throws Exception {

        List<String> resultListRaw = MovieServiceController.getLocationByTitle(title);
        List<String> resultList = new ArrayList<String>();

        for (String str : resultListRaw) {
            
            // step 1: cut off address string after '('
            if (str.indexOf("(") != -1) {
                str = str.substring(0, str.indexOf("("));
            }

            // step 2: replace ' ' with '+'
            str = str.replaceAll(" ", "+");

            // step 3: cut off last '+', if possible
            if (str.length() != 0 && str.charAt(str.length() - 1) == '+') {
                str = str.substring(0, str.length() - 1);
            }
            resultList.add(str);
        }

        JSONObject json = new JSONObject();
        json.put("result", resultList);

        return json.toString();
    }
}
