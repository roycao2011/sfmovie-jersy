/**
 * Copyright (C) 2014 Roy Cao.  All Rights Reserved.
 * Proprietary and confidential.
 * @author  Roy Cao <silencetsao@gmail.com>
 */

package com.roy.workflowservice;

import com.roy.core.MovieKeywordTree;

import java.util.List;
import java.util.LinkedList;

/**
 *	Some basic operations Movie keyword tree
 */
public class MovieKeywordTreeBaseController {

	/**
	 *	Get tree node, given root of the tree and string path.
	 *	
	 *	@param	root tree node to start with
	 *	@param	path or prefix of a string
	 *	@return tree node, path from root node to return node will be given path
	 */
	public static MovieKeywordTree getTreeNodeByPath(MovieKeywordTree root, String path) {
		if (root == null) {
			return null;
		}

		MovieKeywordTree tmpNode = root;
		for (int i = 0; i < path.length(); i++) {
			MovieKeywordTree nextNode = tmpNode.getChildNodesMap().get(path.toLowerCase().charAt(i));
			if (nextNode == null) {
				return null;
			}
			tmpNode = nextNode;
		}
		
		return tmpNode;
	} 

	/**
	 *	Given tree root node, preorder traverse this keyword tree. Print each step.
	 *	
	 *	@param	root tree node
	 */
	public void preorderTraverseTree(MovieKeywordTree root) {
		if (root == null) {
			return;
		}

		if (root.isComplete()) {
			System.out.println(root.getKeyword());
		}

		for (MovieKeywordTree node : root.getChildNodesMap().values()) {
			preorderTraverseTree(node);
		}
	}

	/**
	 *	Given tree root node, get all it's children node's values.
	 *	
	 *	@param	root tree node
	 *	@return list of children node's values as string
	 */
	public List<String> getSubtreeNodeValues(MovieKeywordTree root) {
		List<String> resultList = new LinkedList<String>();

		getSubtreeNodeValuesRecur(resultList, root);

		return resultList;
	}

	/**
	 *	Recursion body for getting child node's values.
	 *	
	 *	@param	List<String>	result list to record string values
	 *	@param	root			root tree node to begin with
	 */
	public void getSubtreeNodeValuesRecur(List<String> result, MovieKeywordTree root) {
		if (root == null) {
			return;
		}

		if (root.isComplete()) {
			result.add(root.getKeyword());
		}

		for (MovieKeywordTree node : root.getChildNodesMap().values()) {
			getSubtreeNodeValuesRecur(result, node);
		}
	}

}