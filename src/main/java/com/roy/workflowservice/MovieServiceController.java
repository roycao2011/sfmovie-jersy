/**
 * Copyright (C) 2014 Roy Cao.  All Rights Reserved.
 * Proprietary and confidential.
 * @author  Roy Cao <silencetsao@gmail.com>
 */

package com.roy.workflowservice;

import com.roy.core.*;
import com.roy.service.NLPService;
import com.roy.service.MovieDataService;

import java.lang.String;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Comparator;
import java.util.Set;
import java.util.HashSet;
 
import javax.net.ssl.HttpsURLConnection;

/**
 *	Some advanced operations on MovieKeywordTree structure.
 */
public class MovieServiceController extends MovieKeywordTreeBaseController {

	//	MovieKeywordTree to play with
	private static MovieKeywordTree tree = null;
	
	//	Map of movie's table to movie object. Since we don't use any db in this project, we will
	//	keep data in memory instead. Whenever server's deployed, we initialize this map.
	private static Map<String, Movie> movieObjects = new HashMap<String, Movie>();
	
	//	Available movie's title list
	private static Set<String> titles = new HashSet<String>();

	public MovieServiceController() {
		this.tree = new MovieKeywordTree();
	}

	/** 
	 *	First time initialize data. Includes:
	 *	1. Get a list of available movie titles.
	 *	2. Make a seperate api call for each movie, load its movie data and initialize the object
	 *	3. Build up a MovieKeywordTree based on above data.
	 */ 
	public static void initMovieData() throws Exception {
		// get title keywords and their frequency
        MovieServiceController.titles = MovieDataService.getSFMovieTitleSet();

        MovieServiceController.initMovieDataObjects();

        List<String> keywords = NLPService.divideBySpaceAll(MovieServiceController.titles);
        Map<String, Integer> keywordFreq = NLPService.getStringFrequency(keywords);

        MovieKeywordTree newTree = MovieServiceController.generateMovieKeywordTree(keywords);
        MovieServiceController.updateKeywordFreq(newTree, keywordFreq);
        MovieServiceController.updateCompleteList(newTree, MovieServiceController.titles);
        MovieServiceController.updateIncompleteListTraverse(newTree, 5);
        MovieServiceController.tree = newTree;
	}

	/** 
	 *	Initialze movie's data, contruct movie's object for each title
	 */ 
	public static void initMovieDataObjects() {
		MovieServiceController.movieObjects = new HashMap<String, Movie>();
		for (String title : titles) {
			Movie newMovie = new Movie(title);
			MovieServiceController.movieObjects.put(title, newMovie);
		}
	}

	/** 
	 *	Update incomplete list for current tree node. We do it recursively, hoping to traverse the tree.
	 *	For each tree node, calling updateIncompleteList() method.
	 *	
	 *	@param	root	MovieKeywordTree root node
	 *	@param	limit	Limit size of incomplete list. We use 5 for this project.
	 */ 
	public static void updateIncompleteListTraverse(MovieKeywordTree root, int limit) {
		if (root == null) {
			return;
		}

		updateIncompleteList(root, limit);
		for (MovieKeywordTree node : root.getChildNodesMap().values()) {
			updateIncompleteListTraverse(node, limit);
		}

		return;
	}

	/**
	 *	Update incomplete list.
	 *	Assumes that each nodes completeList is already updated
	 *	@param	root	MovieKeywordTree root node
	 *	@param	limit	Limit size of incomplete list. We use 5 for this project.
	 */
	public static void updateIncompleteList(MovieKeywordTree root, int limit) {
		Comparator<MovieKeywordTree> comparator = new MovieKeywordTreeFreqComparator();
		PriorityQueue<MovieKeywordTree> queue = new PriorityQueue<MovieKeywordTree>(limit, comparator);

		updateIncompleteListRecur(queue, root);

		List<String> incompleteList = new LinkedList<String>();
		while (incompleteList.size() < limit && !queue.isEmpty()) {
			MovieKeywordTree tmpNode = queue.poll();
			incompleteList.addAll(tmpNode.getCompleteTitles());
		}

		while (incompleteList.size() > limit) {
			incompleteList.remove(limit);
		}
		root.setIncompleteTitles(incompleteList);
	}

	/**
	 *	Recursion body for updating incomplete list.
	 *
	 *	@param	root	MovieKeywordTree root node
	 *	@param	limit	Limit size of incomplete list. We use 5 for this project.
	 */
	public static void updateIncompleteListRecur(PriorityQueue<MovieKeywordTree> queue, MovieKeywordTree root) {
		if (root == null) {
			return;
		}

		queue.add(root);

		for (MovieKeywordTree node : root.getChildNodesMap().values()) {
			updateIncompleteListRecur(queue, node);
		}
	}

	/**
	 *	Update complete list for each node.	Do it recursively to travese the tree.
	 *	@param root		tree root node
	 *	@param titles 	list of all titles
	 */
	public static void updateCompleteList(MovieKeywordTree root, Collection<String> titles) {
		for (String title : titles) {
			updateCompleteList(root, title);
		}
	}

	/**
	 *	Update current title to related tree nodes complete list.
	 *	First divide title to separate words, then go to each word's node,
	 *	update its complete list.
	 *	@param root		tree root node
	 *	@param title 	one movie's title to parse
	 */
	public static void updateCompleteList(MovieKeywordTree root, String title) {
		List<String> words = NLPService.divideBySpace(title);

		for (String word : words) {
			MovieKeywordTree tmpNode = getTreeNodeByPath(root, word);
			tmpNode.getCompleteTitles().add(title);
		}
	}

	/**
	 *	update movie key word tree with string's frequcency, if string is not in the tree,
	 *	do not create a new node.
	 *
	 *	@param root		tree root node
	 *	@param freqMap 	frequency map that stores data
	 */
	public static void updateKeywordFreq(MovieKeywordTree root, Map<String, Integer> freqMap) {
		if (root == null) {
			return;
		}

		for (String str : freqMap.keySet()) {
			MovieKeywordTree tmpNode = getTreeNodeByPath(root, str);
			tmpNode.setFrequency(freqMap.get(str));
		}
	}

	/**
	 * Given a collection of words dictionory, build up MovieKeywordTree based on it.
	 * This is a method to distribute list of titles to tree structure
	 *	
	 * @param   collection of keywords
	 * @return	root node of MovieKeywordTree that is built up
	 */
	public static MovieKeywordTree generateMovieKeywordTree(Collection<String> keywords) {

		MovieKeywordTree root = new MovieKeywordTree();

		for (String keyword : keywords) {
			if (keyword == null) {
				continue;
			}

			MovieKeywordTree tmpNode = root;
			int keywordLength = keyword.length();
			for (int i = 0; i < keywordLength; i++) {
				MovieKeywordTree nextNode = tmpNode.getChildNodesMap().get(keyword.charAt(i));

				if (nextNode == null) {
					MovieKeywordTree newNode = new MovieKeywordTree(keyword.substring(0, i + 1));
					tmpNode.getChildNodesMap().put(keyword.charAt(i), newNode);
					tmpNode = newNode;
				} else {
					tmpNode = nextNode;
				}

				if (i == keywordLength - 1) {
					tmpNode.setComplete(true);
				}
			}
		}

		return root;
	} 

	/**
	 * Search for a list of movie titles given a prefix.
	 * There are two cases:
	 * 1. contains space. This means there has been a complete word.
	 * 2. doesn't contain space. This means there has not been a complete word.
	 *
	 *	@param	Prefix for a movie title
	 *	@return List of movie titles
	 */
	public static List<String> searchMovieByPrefix(String prefix) {
		
		// case 1: prefix string does not contain space, return incomplete titles for current node
		if (prefix.indexOf(' ') == -1) {

			MovieKeywordTree node = getTreeNodeByPath(tree, prefix.toLowerCase());
			if (node == null) {
				return new LinkedList<String>();
			}

			return node.getIncompleteTitles();
		}

		// case 2: prefix string contains space. Find titles that completely contains word one, and check 
		// 		    if the title completely contains prefix. Put the title in only if it does.
		String wordOne = prefix.substring(0, prefix.indexOf(' '));
		MovieKeywordTree node = getTreeNodeByPath(tree, wordOne.toLowerCase());
		if (node == null) {
				return new LinkedList<String>();
		}

		List<String> wordOneTitles = node.getCompleteTitles();
		List<String> resultList = new LinkedList<String>();
		for (String str : wordOneTitles) {
			if (str.toLowerCase().contains(prefix.toLowerCase())) {
				resultList.add(str);
			}
		}

		return resultList;
	}

	/**
	 *	Get all possible locations for a movie title
	 *	
	 *	@param movie title
	 *	@return	list of locations
	 */
	public static List<String> getLocationByTitle(String title) {
		return MovieServiceController.movieObjects.get(title).getLocations();
	}

}