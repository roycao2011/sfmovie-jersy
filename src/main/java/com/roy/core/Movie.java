/**
 * Copyright (C) 2014 Roy Cao.  All Rights Reserved.
 * Proprietary and confidential.
 * @author  Roy Cao <silencetsao@gmail.com>
 */

package com.roy.core;

import com.roy.service.MovieDataService;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.lang.String;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;

import org.json.simple.JSONValue;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;

/**
 *	Defines basic movie class. Contains movie's title, release year, actor, director, writer and
 *	production company's information. Each movie might has multiple locations, so store it as a list
 *	of string.
 */
public class Movie {

	private String title = "";
	private List<String> locations = new LinkedList<String>();
	private String releaseYear = "";
	private String productionCompany = "";
	private String actor_1 = "";
	private String actor_2 = "";
	private String actor_3 = "";
	private String director = "";
	private String writer = "";

	// base url header that queries movie data.
	private static final String MOVIE_API_HEADER = "http://data.sfgov.org/resource/yitu-d5am.json";

	// getter function for location list.
	public List<String> getLocations() {
		return this.locations;
	}

	// constructor, initialize Movie object based on movie's title
	public Movie(String title) {
		this.title = title;
		System.out.println("loading movie's data : " + title);
		initMovie();
	}

	/**
	 *	Initialize movie's data, calling sfData's API to get movie's data by movie's title name.
	 *	By parsing JSON response, store movie's attributes to relative field.
	 */
	public void initMovie() {
		String newUrl = MOVIE_API_HEADER + "?title=" + this.title;
		newUrl = newUrl.replaceAll(" ", "%20");

		try {
			URL url = new URL(newUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

	        StringBuilder out = new StringBuilder();
	        BufferedReader in = new BufferedReader(
			        new InputStreamReader(connection.getInputStream()));
	        String line;
	        while ((line = in.readLine()) != null) {
	            out.append(line);
	        }

	        in.close();

			Object obj=JSONValue.parse(out.toString());
	  		JSONArray array=(JSONArray)obj;

	  		// A list of possible locations
	  		List<String> locations = new LinkedList<String>();

	  		for (Object objectEntry : array) {
	  			JSONObject movieEntry = (JSONObject) objectEntry;
	  			if (movieEntry.containsKey("locations")) {
	  				locations.add(movieEntry.get("locations").toString());
	  			}
	  			if (movieEntry.containsKey("release_year")) {
	  				this.releaseYear = movieEntry.get("release_year").toString();
	  			}
	  			if (movieEntry.containsKey("actor_1")) {
	  				this.releaseYear = movieEntry.get("actor_1").toString();
	  			}
	  			if (movieEntry.containsKey("actor_2")) {
	  				this.releaseYear = movieEntry.get("actor_2").toString();
	  			}
	  			if (movieEntry.containsKey("actor_3")) {
	  				this.releaseYear = movieEntry.get("actor_3").toString();
	  			}
	  			if (movieEntry.containsKey("writer")) {
	  				this.releaseYear = movieEntry.get("writer").toString();
	  			}
	  			if (movieEntry.containsKey("director")) {
	  				this.releaseYear = movieEntry.get("director").toString();
	  			}
	  			if (movieEntry.containsKey("production_company")) {
	  				this.releaseYear = movieEntry.get("production_company").toString();
	  			}
	  		}

	  		// Save locations at the end
	  		this.locations = locations;

  		} catch (Exception e) {
			return;
		}
	}

}