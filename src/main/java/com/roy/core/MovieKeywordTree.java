/**
 * Copyright (C) 2014 Roy Cao.  All Rights Reserved.
 * Proprietary and confidential.
 * @author  Roy Cao <silencetsao@gmail.com>
 */

package com.roy.core;

import java.util.List;
import java.util.Map;
import java.lang.String;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *	Data structure for storing movie's keyword.
 *	Tree structure that makes look up fast. An example tree:
 *				 a
 *			   /   \
 *			  ab    ac
 *			 / \   /  \
 *		   aba abb acc acd
 *	The main advantage for this structure is that finding a given string's input value is relatively easy, just
 *	going from root node, and find the next child by each step's char in the string. The path from the root to 
 * 	current node should be current node's property.	
 *
 *	This is designed for querying movie's title by its name prefix.
 */

public class MovieKeywordTree {
	
	//	Value of current tree node, usually stands for possible prefix of a word
	private String keyword = null;

	//	Map of char to next treenode, indicates which child node should direct to if given next char in string.
	private Map<Character, MovieKeywordTree> childNodesMap = new HashMap<Character, MovieKeywordTree>();
	
	//	Indicates whether current value is already a complete word
	private boolean isComplete = false;

	//	Frequency that current word appears in movie title word dictionary
	private int freq = 0;

	//	A list of titles, which completely contains current prefix. For example, if keyword = "am", then following
	//	might be in this list: {"I am human", "am I right?"}
	private List<String> completeTitles = new LinkedList<String>();

	//	A list of titles, which incompletely contains current prefix. For example, if keyword = "he", the following
	//	might be in this list: {"hers", "the good one"}
	private List<String> incompleteTitles = new LinkedList<String>();

	public MovieKeywordTree() {
		this.keyword = null;
		this.childNodesMap = new HashMap<Character, MovieKeywordTree>();
		this.isComplete = false;
		this.freq = 0;
		this.completeTitles = new LinkedList<String>();
		this.incompleteTitles = new LinkedList<String>();
	}

	public MovieKeywordTree(String keyWord) {
		this.keyword = keyWord;
		this.childNodesMap = new HashMap<Character, MovieKeywordTree>();
		this.isComplete = false;
		this.freq = 0;
		this.completeTitles = new LinkedList<String>();
		this.incompleteTitles = new LinkedList<String>();
	}

	public void setComplete(boolean complete) {
		this.isComplete = complete;
	}

	public boolean isComplete() {
		return this.isComplete;
	}

	public String getKeyword() {
		return this.keyword;
	}

	public void setKeyword(String newKeyword) {
		this.keyword = newKeyword;
	}

	public Map<Character, MovieKeywordTree> getChildNodesMap() {
		return this.childNodesMap;
	}

	public void setFrequency(int frequency) {
		this.freq = frequency;
	} 

	public int getFrequency() {
		return this.freq;
	}

	public List<String> getCompleteTitles() {
		return this.completeTitles;
	}

	public List<String> getIncompleteTitles() {
		return this.incompleteTitles;
	}

	public void setIncompleteTitles(List<String> list) {
		this.incompleteTitles = list;
	}

	public void setCompleteTitles(List<String> list) {
		this.completeTitles = list;
	}

}