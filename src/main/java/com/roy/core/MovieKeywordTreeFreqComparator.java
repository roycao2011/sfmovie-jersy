/**
 * Copyright (C) 2014 Roy Cao.  All Rights Reserved.
 * Proprietary and confidential.
 * @author  Roy Cao <silencetsao@gmail.com>
 */

package com.roy.core;

import java.util.Comparator;

/**
 *	Comparator used when comparing two MovieKeywordTree object. 
 *	Usually, we just compare by their appearance frequcency.
 */
public class MovieKeywordTreeFreqComparator implements Comparator<MovieKeywordTree> {

	@Override
	public int compare(MovieKeywordTree treeNode1, MovieKeywordTree treeNode2) {
		if (treeNode1.getFrequency() > treeNode2.getFrequency()) {
			return -1;
		} 
		if (treeNode1.getFrequency() < treeNode2.getFrequency()) {
			return 1;
		}
		return 0;
	} 
}