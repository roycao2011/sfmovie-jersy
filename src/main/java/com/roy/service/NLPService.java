/**
 * Copyright (C) 2014 Roy Cao.  All Rights Reserved.
 * Proprietary and confidential.
 * @author  Roy Cao <silencetsao@gmail.com>
 */

package com.roy.service;

import java.util.Map;
import java.util.List;
import java.util.Collection;

import java.util.HashMap;
import java.util.LinkedList;

/**
 *	Natural language processing related services. Contains basic string processing methods that 
 *	are helpful for parsing titles.
 */
public class NLPService {

	/**
	 *	Given a list of string, return each string's appearance frequency in the list.
	 *
	 *  @param	list of string (might contains duplicates)
	 *	@return	a map of each string's frequency
	 */
	public static Map<String, Integer> getStringFrequency(List<String> strList) {
		Map<String, Integer> resultMap = new HashMap<String, Integer>();

		for (String s : strList) {
			if (resultMap.containsKey(s)) {
				resultMap.put(s, resultMap.get(s) + 1);
			} else {
				resultMap.put(s, 1);
			}
		}

		return resultMap;
	}

	/**
	 *	Given a list of string, divide each string by space, and return as a whole list.
	 *	Example: input 	{"I am good", "you do"}
	 *			 return {"I", "am", "good", "you", "do"}
	 *
	 *  @param	list of string
	 *	@return	list of word in each given string
	 */
	public static List<String> divideBySpaceAll(Collection<String> list) {
		List<String> resultList = new LinkedList<String>();
		for (String str : list) {
			resultList.addAll(divideBySpace(str));
		}
		return resultList;
	}

	/**
	 *	Given a string, divide the string by space, and return as a list.
	 *	Example: input 	"I am good"
	 *			 return {"I", "am", "good"}
	 *
	 *  @param	string
	 *	@return	list of word in given string
	 */
	public static List<String> divideBySpace(String str) {
		List<String> resultList = new LinkedList<String>();

		int index = 0;
		int startIndex = 0;
		while (index < str.length()) {
			if (index > 0 && str.charAt(index) == ' ') {
				resultList.add(str.substring(startIndex, index).toLowerCase());
				startIndex = index + 1;
				index = index + 1;
			} else {
				index = index + 1;
			}
		}

		if (index != startIndex) {
			resultList.add(str.substring(startIndex, index).toLowerCase());
		}

		return resultList;
	}

}