/**
 * Copyright (C) 2014 Roy Cao.  All Rights Reserved.
 * Proprietary and confidential.
 * @author  Roy Cao <silencetsao@gmail.com>
 */

package com.roy.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.lang.String;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;
import java.util.HashSet;

import org.json.simple.JSONValue;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.net.ssl.HttpsURLConnection;

/**
 *	Services for fetch movie data from external APIs.
 */
public class MovieDataService {
	
  //  API header for sf data public APIs
	private static String SFDataMovieUrlHeader = "http://data.sfgov.org/resource/yitu-d5am.json";

  /**
   *  Get set of all available movie's titles.
   *
   *  @return   set of title strings
   */
	public static Set<String> getSFMovieTitleSet() throws Exception {
		URL url = new URL(SFDataMovieUrlHeader);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		Set<String> titles = new HashSet<String>();

        StringBuilder out = new StringBuilder();
        BufferedReader in = new BufferedReader(
		        new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            out.append(line);
        }

        in.close();

		Object obj=JSONValue.parse(out.toString());
  		JSONArray array=(JSONArray)obj;

  		for (Object objectEntry : array) {
  			JSONObject movieEntry = (JSONObject) objectEntry;
  			titles.add(movieEntry.get("title").toString());
  		}

		return titles;
	}

  /**
   *  Get set of all available movie's directors.
   *
   *  @return   set of title strings
   */
	public static Set<String> getSFMovieDirectorSet() throws Exception {
		URL url = new URL(SFDataMovieUrlHeader);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		Set<String> directors = new HashSet<String>();

        StringBuilder out = new StringBuilder();
        BufferedReader in = new BufferedReader(
		        new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = in.readLine()) != null) {
            out.append(line);
        }

        in.close();

		Object obj=JSONValue.parse(out.toString());
  		JSONArray array=(JSONArray)obj;

  		for (Object objectEntry : array) {
  			JSONObject movieEntry = (JSONObject) objectEntry;
  			directors.add(movieEntry.get("director").toString());
  		}

		return directors;
	}
}